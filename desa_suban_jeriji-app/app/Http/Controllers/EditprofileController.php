<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Ktp;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class EditprofileController extends Controller
{
    public function edit()
    {
        $iduser = Auth::id();
        $detail = Profile::where('user_id', $iduser)->first();
        $mytime = Carbon::now();
        return view('profile.edit', ['detail'=>$detail, 'mytime'=>$mytime]);
    }

    public function editpp()
    {
        $iduser = Auth::id();
        $detail = Profile::where('user_id', $iduser)->first();
        return view('profile.editpp', ['detail'=>$detail]);
    }

    // public function upload($id){
    //     $ktp = Ktp::find($id);
    //     return view('profile.addktp', ['ktp'=>$ktp]);
    // }

    // public function sidebar()
    // {
    //     $iduser = Auth::id();
    //     $detail = Profile::where('user_id', $iduser)->first();
    //     return view('partials.sidebar', ['detail'=>$detail]);
    // }
}
